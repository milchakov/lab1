package com.company;

//import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.io.*;


public class Main {
    public static boolean isNull(binSTree b) {
        return (b == null);
    }

    public static int RootBT(binSTree b)            // ��� ��������� ���.������
    {
        return b.info;
    }

    //-------------------------------------
    /*public static int RootCountBT (binSTree b)// ��� ��������� ���.������
    {
        return b.count;
    }*/
    //-------------------------------------
    public static binSTree Left(binSTree b)        // ��� ��������� ���.������
    {
        return b.lt;
    }

    //-------------------------------------
    public static binSTree Right(binSTree b)        // ��� ��������� ���.������
    {
        return b.rt;
    }
    //-------------------------------------

    public static binSTree rotateR(binSTree t)
    //b - не пусто
    {
        binSTree x = new binSTree();
        x = t.lt;
        t.lt = x.rt;
        x.rt = t;
        t = x;
        return t;

    }

    public static binSTree rotateL(binSTree t)
    //b - не пусто
    {
        binSTree x = new binSTree();
        x = t.rt;
        t.rt = x.lt;
        x.lt = t;
        t = x;
        return t;

    }

    public static binSTree RandomInsert(int x, binSTree b) {
        if (b == null) {
            b = new binSTree();

            if (b != null) {
                b.info = x;
                b.count = 1;
                b.lt = null;
                b.rt = null;

            }
        }
        if (random(b)) {
            b = insInRoot(b, x);
        } else {
            if (x < b.info) {
                b.lt = RandomInsert(x, b.lt);
            }
            if (x > b.info) {
                b.rt = RandomInsert(x, b.rt);
            }
            if (x == b.info) {
                b.count++;
            }
        }
        return b;
    }

    public static binSTree insInRoot(binSTree b, int x) {
        if (b == null) {
            b = new binSTree();
            if (b != null) {
                b.info = x;
                b.count = 1;
                b.lt = null;
                b.rt = null;

            }

        } else {
            if (x < b.info) {
                b.lt = insInRoot(b.lt, x);
                b = rotateR(b);
            } else if (x > b.info) {
                b.rt = insInRoot(b.rt, x);
                b = rotateL(b);
            } else {
                b.count++;
            }
        }
        return b;
    }

    public static boolean random(binSTree b) {
        Random rand = new Random();
        int qwe = sizeBT(b);
        return rand.nextInt(qwe) < 1;
        //return rand()%(sizeBT(b))<1;
    }
    //-------------------------------------


    public static int hBT(binSTree b) {
        if (isNull(b)) return 0;
        else return max(hBT(Left(b)), hBT(Right(b))) + 1;
    }

    public static int sizeBT(binSTree b) {
        if (isNull(b)) return 0;
        else return sizeBT(Left(b)) + sizeBT(Right(b)) + 1;
    }

    public static int max(int y, int x) {
        if (y > x) {
            return y;
        } else {
            return x;
        }
    }

    public static void displayBT(binSTree b, int n) {    // n - ������� ����
        if (b != null) {
            System.out.print(" " + RootBT(b));
            if (!isNull(Right(b))) {
                displayBT(Right(b), n + 1);
            } else System.out.println(); // ����
            if (!isNull(Left(b))) {
                for (int i = 1; i <= n; i++) System.out.print("  "); // ������
                displayBT(Left(b), n + 1);
            }
        }

    }

    /*public  static  void printKLP (binSTree b)
    {	if (!isNull(b))
        {
            System.out.print(RootBT(b));
            printKLP (Left(b));
            printKLP (Right(b));
        }
    }

    public  static void printLKP (binSTree b)
    {	if (!isNull(b))
        {
            printLKP (Left(b));
            System.out.print(   RootBT(b) + "(" + RootCountBT(b)+ ")" );
            printLKP (Right(b));
        }
    }*/

//
    public static void printLKPInFile(binSTree b, PrintWriter file) throws IOException {
        if (!isNull(b)) {
            printLKPInFile(Left(b), file);
            String qw = String.valueOf(RootBT(b));
            int qwe = RootBT(b);

            file.write(qwe + " ");
//            System.out.print(   RootBT(b) + "(" + RootCountBT(b)+ ")" );
            printLKPInFile(Right(b), file);
        }
    }

    /*public  static void printLPK (binSTree b)
    {
        if (!isNull(b))
        {
            printLPK (Left(b));
            printLPK (Right(b));
            System.out.print(RootBT(b));
         }
    }*/


    public static void main(String[] args) throws IOException {
        binSTree b = new binSTree();
        FileInputStream fileInputStream = new FileInputStream("data.txt");
        Scanner scanner = new Scanner(fileInputStream);
        b = null;
        int k = 0;
        while(scanner.hasNext())
        {
            int data = scanner.nextInt();
            b = RandomInsert(data, b);
            k++;
        }
        fileInputStream.close();
        System.out.println("symbols " + k);
        System.out.println("tree " + hBT(b));
        System.out.println("ys tree " + sizeBT(b));
        displayBT(b, 1);
        System.out.println();
        fileInputStream.close();


        PrintWriter writer = new PrintWriter("out.txt", "UTF-8");


       /* printLKP (b);
        System.out.println();
        printKLP (b);
        System.out.println();
        printLPK (b);*/

//        String s = BinSTreeToString(b);

//        writer.write(s);

        printLKPInFile(b,writer);
        writer.close();


    }


}
